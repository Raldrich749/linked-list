public class LList<X>
{
    private Node<X> head;
    
    public LList()
    {
        head = null;
    }
    
    /**
     * Add a new node at the head of the list.
     */
    public void addFirst(X value)
    {
        if(head==null)
        {
             head = new Node();
             head.data = value;
        }
        else
        {
            Node a = new Node();
            a.data = value;
            Node t = head;
            a.next = t;   
            head = a;
        }
    }
    
    /**
     * Return the number of nodes in the list.
     */
    public int size()
    {
        Node t = head;
        int count = 0;
        
        while(t!= null)
        {
             count++;
             t = t.next;
        }
        return count;
    }
    
    /**
     * Return the value found at the node whose position
     * is given by the index.
     * Thrown an ICantEven exception if the index is out
     * of bounds.
     */
    public Object get(int index)
    {
        if(index<0||index>size()-1)
        {
            throw new ICantEven();
        }
        else
        {
            Node t = head;
            for(int i = 0; i < index; i++)
            {
                t = t.next;
            }
        
            return t.data;
        }
   }
    
    /**
     * Delete the head of the list. Throw an ICanEven
     * exception if the list is empty.
     */
    public void removeFirst()
    {
        if(head==null)
        {
            throw new ICantEven();
        }
        else
        {
            Node t = head;
            t=t.next;
            head = t;
        }
    }
    
    public void remove(int index)
    {
        if(index<0||index>size()-1)
        {throw new ICantEven();}
        else if(index==0)
        {
            removeFirst();
        }
        else
        {
        Node t = head;
        for(int i = 0; i <index-1; i++)
        {
            t=t.next;
        }
        t.next=t.next.next;
    }
    }
    
    public void removeLast()
    {
        if(head==null)
        {
            throw new ICantEven();
        }
        else if(size()==1)
        {
            head = null;
        }
        else
        {
            Node t = head;
            while(t.next.next!=null)
            {
                t=t.next;
            }
            t.next=null;
        }
    }
    
    /**
     * Return a string containing the values found in each
     * node. The format is the same as for the AL class:
     * "[5, 6, 1, 2]"
     * Return just a set of brackets "[]" if the list is
     * empty.
     */
    public String toString()
    {
        String a = "[";
        Node t = head;
        for(int i = 0; i < size(); i++)
        {
            if(i<size()-1)
            {
                a+=t.data + ", ";
            }
            else
            {
                a+=t.data;
            }
            t=t.next;
        }
        a+="]";
        return a;
    }
    
    /**
     * Insert a new node at the given index.
     * Throw an ICantEven exception if the index is out of
     * bounds.
     */
    public void insert(int index, X value)
    {
        if(index<0 || index > size())
        {
            throw new ICantEven();
        }
        else
        {
            Node newNode = new Node();
            newNode.data = value;
            Node t = head;
            if(index == 0)
            {
                addFirst(value);
            }
            else
            {
               for(int i = 0; i <index-1; i++)
               {
                    t=t.next;
               }
                newNode.next=t.next;
                t.next=newNode;
            }
        }
    }
    
    public void addLast(X value)
    {
        if(head==null)
        {
            addFirst(value);
        }
        else
        {
            Node newNode = new Node();
            newNode.data=value;
            Node t = head;
            while(t.next!=null)
            {
                t=t.next;
            }      
            t.next=newNode;
        }
    }
    
    public void clear()
    {
        head = null;
    }
    
    public void add(X value)
    {
        addLast(value);
    }
    
    public void set(int index, X value)
    {
        if(index<0||index>size()-1)
        {
            throw new ICantEven();
        }
        else
        {
            if(index==0)
            {
                head.data=value;
            }
            else
            {
                Node t = head;
                for(int i = 0; i <index-1; i++)
               {
                   t=t.next;
               }
                t.next.data=value;
           }
        }
    }
    
    public int indexOf(X value)
    {
        Node t = head;
        int a = -1;
        for(int i=0; i<size();i++)
        {
            if(t.data==value||t.data.equals(value))
            {
                a = i;
                i=size();
            }
            t=t.next;
        }
        return a;
    }
}